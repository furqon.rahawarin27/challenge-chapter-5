const express = require('express')
const app = express()
const user = require('./router/user')
app.use('/public', express.static('public'));

app.use(express.json())
app.use(express.urlencoded({extended: true}))
app.set('view engine', 'ejs')
app.use(express.static('public'))

var Login = function (req, res, next) {
  res.render('login.ejs')
  next()
}

app.get('/', (req, res) => {
  res.render('index.ejs')
});

app.get('/login', (req, res) => {
  res.render('login.ejs')
});

// app.use(Login)
app.get('/play', (req, res) => {
  res.render('play.ejs')
});

app.use(user)

app.listen(3000, () => {
  console.log('Server started on port')
})