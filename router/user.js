const express = require('express')
const router = express.Router()

let users = [
    {
        id : 1,
        name : "Admin",
        email : "admin@gmail.com"
    },
    {
        id : 2,
        name : "User",
        email : "user@gmail.com"
    }
]

router.get('/api/v1/user', (req, res)=>{
    res.json(users)
});

router.post('/api/v1/register', (req, res) => {
    const {name, email} = req.body

    if(name === undefined || email === undefined){
        res.status(400).json("Bad request because some data is not sent")
        return
    }
    const nextId = users.length + 1

    users.push({
        id : nextId,
        name : name,
        email : email
    })

    res.status(200).json("Succeed!")
});

router.post('/api/v1/login', (req, res) => {
    const {name, email} = req.body

    if(name === undefined || email === undefined ){
        res.status(400).json("Bad request because some data is not sent")
        return
    }

    let isFound = false
    for(var x = 0; x<users.length; x++){
        if(users[x].name == name){
            users[x].email = email

            isFound = true
            break
        }
    }

    if(isFound == false){
        res.status(404).json("Gagal Login")
        return
    }else{
        res.status(200).json("Hallo " + name)
        return
    }

});

module.exports = router